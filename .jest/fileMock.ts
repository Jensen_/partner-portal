import * as path from "path";

module.exports = {
  process(src: any, filename: string) {
    return `module.exports = ${JSON.stringify(path.basename(filename))};`;
  },
};
