module.exports = {
  "**/*.{ts,tsx,css}": "prettier --write",
  "*.css": "stylelint --cache --fix",
  "**/*.ts?(x)": [
    "eslint ./src --cache --fix --ext .tsx --ext .ts",
    () => "tsc -p tsconfig.json --noEmit",
    () => "jest --bail --onlyChanged",
  ],
};
