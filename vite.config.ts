import reactRefresh from "@vitejs/plugin-react-refresh";
import { defineConfig, UserConfig } from "vite";
import react from "@vitejs/plugin-react";
import tsconfigPaths from "vite-tsconfig-paths";
import checker from "vite-plugin-checker";

// https://vitejs.dev/config/
export default defineConfig(({ mode }: UserConfig) => {
  const config: UserConfig = {
    plugins: [
      tsconfigPaths(),
      checker({
        typescript: true,
        overlay: { position: "bl", initialIsOpen: true, badgeStyle: "margin: 0 0 15px 60px" }, // prevent overlap  with react-query devtool
        eslint: {
          lintCommand: "eslint ./src/**/*.{ts,tsx} --cache",
        },
      }),
      react(),
      reactRefresh(),
    ],
    /*server: {
      proxy: {
        "/api": {
          target: "http://localhost:8080",
          secure: false,
          rewrite: (path) => path.replace(/^\/api/, ""),
        },
      },
    },*/
  };
  return config;
});
