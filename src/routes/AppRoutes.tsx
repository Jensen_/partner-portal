import { BrowserRouter, Route, Routes } from "react-router-dom";
import { VFC } from "react";
import LoginPage from "pages/login/LoginPage";
import NotFoundPage from "pages/NotFoundPage";
import MainLayout from "layout/MainLayout";
import DashboardPage from "pages/dashboard/DashboardPage";
import TaskListPage from "pages/project/TaskListPage";
import ServerErrorPage from "pages/ServerErrorPage";

const AppRoutes: VFC = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<MainLayout />}>
          <Route index element={<DashboardPage />} />
          <Route path="/dashboard" element={<DashboardPage />} />
          <Route path="/task/list/*" element={<TaskListPage />} />
        </Route>
        <Route path="/login" element={<LoginPage />} />
        <Route path="/error/404" element={<NotFoundPage />} />
        <Route path="/error/500" element={<ServerErrorPage />} />
        <Route path="*" element={<NotFoundPage />} />
      </Routes>
    </BrowserRouter>
  );
};

export default AppRoutes;
