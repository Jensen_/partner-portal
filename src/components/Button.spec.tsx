import { fireEvent } from "@testing-library/react";
import Button from "components/Button";
import { render } from "test-utils";

describe("Button", () => {
  it("should render button", () => {
    const onClick = jest.fn();
    const { getByDataTest } = render(<Button onClick={onClick} />);
    const button = getByDataTest("bt-ok");
    fireEvent.click(button);
    expect(onClick).toHaveBeenCalledTimes(1);
  });
});
