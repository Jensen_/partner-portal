import styles from "components/button.module.css";
import { FC } from "react";

interface Props {
  test?: string;
  onClick: () => void;
}

const Button: FC<Props> = ({ onClick, test = "true" }: Props) => {
  console.log(test);
  return (
    <button data-test={"bt-ok"} type="submit" className={styles["button-ok"]} onClick={onClick}>
      Hello word
    </button>
  );
};

export default Button;
