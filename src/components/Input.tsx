import { ExclamationCircleIcon } from "@heroicons/react/outline";
import { FC } from "react";

type Props = {
  id: string;
  value?: string;
  label: string;
  error?: string;
  type: "email" | "password";
  required?: boolean;
};

const Input: FC<Props> = ({ id, type, value = "", label, error, required = true }: Props) => {
  const hasError = typeof error === "string" && error.length > 0;
  return (
    <div>
      <label htmlFor={id} className="block text-sm font-medium text-gray-700">
        {label}
      </label>
      <div className="mt-1">
        <div className={hasError ? "relative rounded-md shadow-sm" : ""}>
          <input
            id={id}
            name={type}
            type={type}
            autoComplete={`current-${type}`}
            defaultValue={value}
            required={required}
            className={
              hasError
                ? "block w-full rounded-md border-red-300 pr-10 text-red-900 placeholder-red-300 focus:border-red-500 focus:outline-none focus:ring-red-500 sm:text-sm"
                : "block w-full appearance-none rounded-md border border-gray-300 px-3 py-2 placeholder-gray-400 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
            }
          />
          {hasError ? (
            <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-3">
              <ExclamationCircleIcon className="h-5 w-5 text-red-500" aria-hidden="true" />
            </div>
          ) : null}
        </div>
        {hasError ? (
          <p className="mt-2 text-sm text-red-600" id="email-error">
            {error}
          </p>
        ) : null}
      </div>
    </div>
  );
};

export default Input;
