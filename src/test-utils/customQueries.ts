import { buildQueries, queryHelpers } from "@testing-library/react";
import { Matcher, MatcherOptions } from "@testing-library/dom/types/matches";
import { GetErrorFunction } from "@testing-library/dom/types/query-helpers";

const queryAllByDataTest = (container: HTMLElement, id: Matcher, options?: MatcherOptions) =>
  queryHelpers.queryAllByAttribute("data-test", container, id, options);

const getMultipleError: GetErrorFunction = (element, dataCyValue) =>
  `Found multiple elements with the data-cy attribute of: ${dataCyValue}`;

const getMissingError: GetErrorFunction = (element, dataCyValue) =>
  `Unable to find an element with the data-cy attribute of: ${dataCyValue}`;

const [queryByDataTest, getAllByDataTest, getByDataTest, findAllByDataTest, findByDataTest] = buildQueries(
  queryAllByDataTest,
  getMultipleError,
  getMissingError
);

export { queryByDataTest, queryAllByDataTest, getByDataTest, getAllByDataTest, findAllByDataTest, findByDataTest };
