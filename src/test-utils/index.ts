import { queries, render, RenderOptions } from "@testing-library/react";
import * as customQueries from "./customQueries";
import { ReactElement } from "react";

const customRender = (ui: ReactElement, options?: Omit<RenderOptions, "queries">) =>
  render(ui, { queries: { ...queries, ...customQueries }, ...options });

export { customRender as render };
