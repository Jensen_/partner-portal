import { StrictMode } from "react";
import { render } from "react-dom";
import "./index.css";
import ReactQueryProvider from "providers/ReactQueryProvider";
import AppRoutes from "routes/AppRoutes";

render(
  <StrictMode>
    <ReactQueryProvider>
      <AppRoutes />
    </ReactQueryProvider>
  </StrictMode>,
  document.getElementById("root")
);
