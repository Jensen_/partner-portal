import { FC, ReactNode } from "react";

interface Props {
  title: string;
  children?: ReactNode;
}

const Page: FC<Props> = ({ title, children }: Props) => {
  return (
    <>
      <header className="border-t bg-white py-10 shadow">
        <div className="px-4 sm:px-6 lg:px-8">
          <h1 className="text-3xl font-medium leading-tight text-gray-900">{title}</h1>
        </div>
      </header>
      <main>
        <div className="sm:px-6 lg:px-8">{children}</div>
      </main>
    </>
  );
};

export default Page;
