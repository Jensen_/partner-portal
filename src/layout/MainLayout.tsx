import { VFC } from "react";
import { Outlet } from "react-router-dom";
import PageNavBar from "layout/PageNavBar";

const MainLayout: VFC = () => {
  return (
    <>
      <div className="min-h-full">
        <PageNavBar />
        <div className="">
          <Outlet />
        </div>
      </div>
    </>
  );
};

export default MainLayout;
