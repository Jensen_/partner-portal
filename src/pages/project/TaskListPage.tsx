import { VFC } from "react";
import { NavLink } from "react-router-dom";

const tabs = [
  { name: "Offers", href: "/task/list/offers" },
  { name: "Closed", href: "/task/list/upcoming" },
  { name: "Ongoing", href: "/task/list/ongoing" },
  { name: "Completed", href: "/task/list/completed" },
];
function classNames(...classes: string[]) {
  return classes.filter(Boolean).join(" ");
}

const TaskListPage: VFC = () => {
  return (
    <>
      <header className="border-t bg-white pt-5 shadow">
        <div className="px-4 sm:px-6 lg:px-8">
          <h1 className="text-3xl font-medium leading-tight text-gray-900">Tasks</h1>
          <div className="border-b border-gray-200 pt-5">
            <div className="sm:flex sm:items-baseline">
              <nav className="-mb-px flex space-x-8">
                {tabs.map((tab) => (
                  <NavLink
                    key={tab.name}
                    to={tab.href}
                    className={({ isActive }) =>
                      classNames(
                        isActive
                          ? "border-indigo-500 text-indigo-600"
                          : "border-transparent text-gray-500 hover:border-gray-300 hover:text-gray-700",
                        "whitespace-nowrap border-b-2 px-1 pb-4 text-sm font-medium"
                      )
                    }
                  >
                    {tab.name}
                  </NavLink>
                ))}
              </nav>
            </div>
          </div>
        </div>
      </header>
      <main>
        <div className="sm:px-6 lg:px-8" />
      </main>
    </>
  );
};

export default TaskListPage;
