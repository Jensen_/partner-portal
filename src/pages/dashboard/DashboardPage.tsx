import { VFC } from "react";
import Page from "layout/Page";

const DashboardPage: VFC = () => {
  return (
    <Page title={"Dashboard"}>
      <div className="px-4 py-8 sm:px-0">
        <h3>Dashboard</h3>
      </div>
    </Page>
  );
};

export default DashboardPage;
