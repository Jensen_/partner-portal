import { XCircleIcon } from "@heroicons/react/outline";
import { VFC } from "react";
import { Link } from "react-router-dom";
import Input from "components/Input";

const actionData = {
  fields: { email: "" },
  fieldErrors: { email: undefined, password: undefined },
  formError: false,
  csrf: "",
};

const LoginPage: VFC = () => {
  return (
    <div className="flex min-h-full flex-col">
      <main className="flex min-h-full flex-auto flex-col justify-center py-12 sm:px-6 lg:px-8">
        <div className="sm:mx-auto sm:w-full sm:max-w-md">
          <img className="mx-auto h-12 w-auto" src="/logo.png" alt="Acolad" />
          <h2 className="mt-6 text-center text-3xl font-medium text-gray-900">Sign in to your account</h2>
        </div>

        <div className="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
          <div className="bg-white py-8 px-4 shadow sm:rounded-lg sm:px-10">
            <form className="space-y-6" method="post">
              <input id="csrf-input" name="csrf" type="hidden" value={actionData.csrf} />
              <div>
                <Input
                  id="email-input"
                  type="email"
                  label="Email address"
                  value={actionData?.fields?.email}
                  error={actionData?.fieldErrors?.email}
                />
              </div>
              <div>
                <Input id="password-input" type="password" label="Password" error={actionData?.fieldErrors?.password} />
              </div>
              <div className="flex items-center justify-between">
                <div className="flex items-center">
                  <input
                    id="remember-me"
                    name="rememberMe"
                    type="checkbox"
                    checked={true}
                    readOnly={true}
                    className="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500"
                  />
                  <label htmlFor="remember-me" className="ml-2 block text-sm text-gray-900">
                    Remember me
                  </label>
                </div>

                <div className="text-sm">
                  <Link to={"."} className="font-medium text-indigo-600 hover:text-indigo-500">
                    Forgot your password?
                  </Link>
                </div>
              </div>
              {actionData?.formError ? (
                <div className="rounded-md bg-red-50 p-4 px-4">
                  <div className="flex">
                    <div className="shrink-0">
                      <XCircleIcon className="h-5 w-5 text-red-400" aria-hidden="true" />
                    </div>
                    <div className="ml-3">
                      <h3 className="text-sm font-medium text-red-800"> {actionData.formError}</h3>
                    </div>
                  </div>
                </div>
              ) : null}
              <div>
                <button
                  type="submit"
                  className="flex w-full justify-center rounded-md border border-transparent bg-indigo-600 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                >
                  Sign in
                </button>
              </div>
            </form>
          </div>
        </div>
      </main>
      <footer className="mx-auto flex w-full max-w-full items-center justify-center py-10 text-center text-sm text-gray-500">
        <p>© {new Date().getFullYear()} Acolad Inc. All rights reserved.</p>
      </footer>
    </div>
  );
};

export default LoginPage;
