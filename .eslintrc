{
  "env": {
    "browser": true,
    "es2021": true,
    "jest": true,
    "node": true
  },
  "settings": {
    "react": {
      "version": "detect"
    },
    "import/resolver": {
      "typescript": {} // this loads <rootdir>/tsconfig.json to eslint
    },
    "tailwindcss": {
      "officialSorting": true
    }
  },
  "extends": [
    "eslint:recommended",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:import/recommended",
    "plugin:import/typescript",
    "plugin:react/recommended",
    "plugin:react-hooks/recommended",
    "plugin:tailwindcss/recommended",
    "prettier"
  ],
  "parser": "@typescript-eslint/parser",
  "parserOptions": {
    "ecmaFeatures": {
      "jsx": true
    },
    "ecmaVersion": 11,
    "sourceType": "module"
  },
  "plugins": ["react", "react-hooks", "@typescript-eslint", "tailwindcss", "unicorn"],
  "rules": {
    "@typescript-eslint/ban-ts-comment": "warn",
    "comma-dangle": "off",
    "multiline-ternary": "off",
    "no-use-before-define": "off",
    "space-before-function-paren": "off",
    "react/prop-types": "off",
    "react/no-unescaped-entities": "off",
    "react/display-name": "off",
    "react/react-in-jsx-scope": "off",
    "react-hooks/rules-of-hooks": "error",
    "react-hooks/exhaustive-deps": "warn",
    "tailwindcss/classnames-order": "warn",
    "tailwindcss/no-custom-classname": "warn",
    "tailwindcss/no-contradicting-classname": "error",
    "no-restricted-imports": [
      "error",
      {
        "paths": [
          {
            "name": "react",
            "importNames": ["default"],
            "message": "Do not import React, use import deconstruction instead"
          }
        ]
      }
    ]
  },
  "overrides": [
    {
      "files": ["src/**/*.ts?(x)"],
      "parserOptions": {
        "project": ["./tsconfig.json"]
      }
    },
    {
      "files": ["vite.config.ts"],
      "parserOptions": {
        "project": ["./tsconfig.node.json"]
      }
    },
    {
      "files": ["**/**/*.(test|spec).ts?(x)"],
      "extends": ["plugin:testing-library/react"],
      "rules": {
        "testing-library/no-await-sync-events": "error",
        "testing-library/no-manual-cleanup": "error",
        "testing-library/prefer-explicit-assert": "error",
        "testing-library/prefer-user-event": "error",
        "testing-library/prefer-wait-for": "error"
      }
    }
  ]
}
